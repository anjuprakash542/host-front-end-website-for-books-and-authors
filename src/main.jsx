import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import './index.css'
import Root from './routes/root';
import ErrorPage from './error-page';
import Home from './routes/home';
import Authors,{loader as authorsLoader} from './routes/authors';
import BooksForSingleAuthor,{loader as BookAuthorLoader} from './routes/singleAuthorBooks';
import Books,{loader as booksLoader} from './routes/books';
import SingleNovel,{loader as singleNovelLoader} from './routes/singleNovel';
import Signup from './routes/signup';
import Login from './routes/login';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root/>,
    errorElement:<ErrorPage/>,
    children : [
      {
        path:"/",
        element:<Home/>,
      },
      {
        path:"/authors",
        element:<Authors/>,
        loader:authorsLoader
      },
      {
        path:"/authors/:authorId",
        element:<BooksForSingleAuthor/>,
        loader:BookAuthorLoader
      },
      {
        path:"/books",
        element:<Books/>,
        loader:booksLoader
      },
      {
        path:"/books/:bookId",
        element:<SingleNovel/>,
        loader:singleNovelLoader
      },
      {
        path:"/signup",
        element:<Signup/>
      },
      {
        path:"/login",
        element:<Login/>
      }
    ]
  },
  {
    
  }
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
   <RouterProvider router={router} />
  </React.StrictMode>,
)
