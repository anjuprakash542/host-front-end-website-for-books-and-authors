import axios from "axios";
import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom";

export default function Login (props){
    useEffect(() => {
        document.body.style.backgroundImage = null;
        document.body.style.backgroundSize = null;
        document.body.style.backgroundRepeat = null;
    }, []);

    const [email, setEmail] = useState('')
    const [password,setPassword] =useState('')
    const navigate = useNavigate()
  
    function handleLogin(loginEvent) {
        loginEvent.preventDefault()
        const data  ={
           
            email:email,
            password:password
           }
      axios.post (`${import.meta.env.VITE_API}/auth/login`,data,{withCredentials:true})
      .then (response => {
          navigate('/')
      })

      .catch(error => {
        console.log(error)
      }
        
      )
    }
 return(

    
    <>
        <main>
        <section className="container mx-auto py-20">
            <h1 className="text-2xl font-bold mb-4">
                Login
            </h1>
            <form onSubmit={handleLogin} className="flex flex-col gap-4">
               
                <label htmlFor="email">Email</label>
                <input onChange={(input)=>setEmail(input.target.value)} className="border border-indigo-300 rounded-sm" type="email" name="email" id="email" />

                <label htmlFor="password"> Password </label>
                <input onChange={(input)=> setPassword(input.target.value)} className="border border-indigo-300 rounded-sm" type="password" name="passwrod" id="password" />

                <button className="p-2 bg-indigo-800 text-white font-bold rounded-sm mt-8 hover:bg-indigo-600" type="submit">Login</button>
            </form>
        </section>
    </main>
    </>
 )
}