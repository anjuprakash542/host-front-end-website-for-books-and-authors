import axios from "axios";
import { useEffect, useState } from "react"

export default function Signup (props) {
    useEffect(() => {
        document.body.style.backgroundImage = null;
        document.body.style.backgroundSize = null;
        document.body.style.backgroundRepeat = null;
    }, []);

    const [name,setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    function handleSubmit(submitEvent){
         submitEvent.preventDefault();
         const data = {
            name:name,
            email:email,
            password:password
         }
        axios.post(`${import.meta.env.VITE_API}/users`,data)
        .then(response => console.log(response))
        .catch(error => console.log(error))
    }
    return (
        <>

<main>
        <section className="container mx-auto py-20">
            <h1 className="text-2xl font-bold">
                Signup
            </h1>
            <form onSubmit={handleSubmit}  className="flex flex-col gap-4">
                <label htmlFor="name"> Name </label>
                <input onChange={(input)=>setName(input.target.value)} className="border border-indigo-300 rounded-sm" type="text" id="name" />

                <label htmlFor="email">Email</label>
                <input onChange={(input)=>setEmail(input.target.value)} className="border border-indigo-300 rounded-sm" type="email" name="email" id="email" />

                <label htmlFor="password"> Password </label>
                <input onChange={(input)=> setPassword(input.target.value)} className="border border-indigo-300 rounded-sm" type="password" name="passwrod" id="password" />

                <button className="p-2 bg-indigo-800 text-white font-bold rounded-sm mt-8 hover:bg-indigo-600" type="submit">Signup</button>
            </form>
        </section>
    </main>
        </>
    )
}