import { useEffect } from "react";
import { Link } from "react-router-dom";

export default function Home (props) {
    useEffect(() => {
        document.body.style.backgroundImage = 'url("https://imgs.6sqft.com/wp-content/uploads/2016/10/14160508/nypl-book-stacks1.jpg")';
        document.body.style.backgroundSize = 'cover';
        document.body.style.backgroundRepeat = 'no-repeat';
        document.body.style.backgroundPosition = 'center';
        // Cleanup to reset the background color when the component unmounts
        return () => {
          document.body.style.backgroundColor = '';
          document.body.style.backgroundSize = '';
          document.body.style.backgroundRepeat = '';
          document.body.style.backgroundPosition = '';

        };
      }, []);

      
    return (
        <>
         <div className=" firstHomeDiv h-full w-11/12 m-auto md:py-10 mt-5 "> 
           <article className=" homeArticle text-center w-8/12 container mx-auto font-bold text-white h-56">
              <h2 className="homeH2 text-4xl  mb-3  w-6/12 m-auto"> Discover Your Next Favorite Book </h2>
              <p className=" w-6/12 m-auto">
              Explore a curated collection of the most popular books and learn about their acclaimed authors. Whether you’re looking for a classic masterpiece or the latest bestseller, you’ll find something that sparks your interest.
              </p>
           </article>
           <div >
           <div className="grid grid-cols-2 gap-6 container  font-bold text-center  text-2xl mt-6">
              <Link to={'/authors'}>
                <div className="bg-orange-500 py-1">Authors Corner</div>
              </Link>
               <Link to ={'/books'}>
                 <div className="bg-orange-500 py-1">Books Corner</div>
               </Link>
              
           </div>
           </div>
           

          
         </div>
        </>
    )
}