import { useEffect } from "react";
import { Link, useLoaderData } from "react-router-dom";

export async function loader() {
    const response = await fetch(`${import.meta.env.VITE_API}/books`);
    const books = await response.json()
    return { books };
  }

export default function Books (props) {

    useEffect(() => {
        document.body.style.backgroundImage = null;
        document.body.style.backgroundSize = null;
        document.body.style.backgroundRepeat = null;
    }, []);

    const {books} = useLoaderData()

    return (
        <>
         <div>
            <ul className="grid grid-cols-4 grid-rows-2 gap-2 mt-5 ml-5 mr-5">
                {
                    books.map (book => {
                        return(
                            <Link to={`/books/${book._id}`}>
                            <li className="bg-orange-100 w-10/12 h-8/12" key={book._id}>
                                <h3 className="text-center font-bold text-xl">{book.bookName}</h3>
                                
                                <img src={book.image} className="w-full h-full"></img>
                              
                               
                                <span className="font-bold"> Written by - &nbsp; <span>{book.author}</span></span>
                            </li>
                            </Link>
                        )
                    })
                }
            </ul>
         </div>
        </>
    )
}