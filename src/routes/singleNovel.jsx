import { useLoaderData } from "react-router-dom";

export async function loader({params}) {
    const response = await fetch(`${import.meta.env.VITE_API}/books/${params.bookId}`);
    const books = await response.json()
    return { books };
  }
export default function SingleNovel (props) {
    const {books} = useLoaderData()
    return(
        <>
        <div className=" container mx-auto py-10 flex flex-row">
                   <div className="flex flex-col w-full h-full">
                    <div className="">
                        <img src={books.image} className="w-full h-full" alt="" />
                    </div>

                    <div className="text-center font-bold">
                        <h3> Book Name : {books.bookName}</h3>
                        <h3> Author : {books.author}</h3>
                    </div>
                   </div>
                   <div className=" font-bold text-slate-500 flex flex-row justify-center items-center ml-10 "> {books.description}</div>
                    
           
        </div>

        </>
    )
}