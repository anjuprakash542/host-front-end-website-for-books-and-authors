import { Link, Outlet } from 'react-router-dom';

export default function Root (props) {
    return (
        <>
          <header className='shadow-xl h-20 bg-white' style={{ minWidth: '550px' }}>
            <div className='container mx-auto py-6 flex flex-row justify-between items-center'>
               
                <h1 className='myh1 font-bold'>BOOK-STALL</h1>

                <nav>
                    <ul className='flex flex-row gap-6 font-bold text-gray-600'>
                        <li>
                            <Link to ={"/"}> Home </Link>
                        </li>

                        <li>
                            <Link to ={"/books"}> Books </Link>
                        </li>

                        <li>
                            <Link to ={"/authors"}> Authors </Link>
                        </li>
                          
                        <li>
                            <Link to ={"/signup"}> Signup </Link>
                        </li>

                            
                        <li>
                            <Link to ={"/login"}> Login </Link>
                        </li>
                          

                        <li>
                            <Link to ={"/contacts"}> Contacts </Link>
                        </li>
                    </ul>
                </nav>
            </div>

        </header>
        <Outlet/>
        <footer className='myFooter h-14 w-full bg-slate-300 '>
            <section className=' container mx-auto flex flex-row justify-center gap-6 mt-3'>
            <a href="https://www.facebook.com" target="_blank" rel="noopener noreferrer">
                  <img width="30" height="30" src="https://img.icons8.com/office/30/facebook-new.png" alt="facebook-new"/>
            </a>
            
            <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                  <img width="30" height="30" src="https://img.icons8.com/officel/30/twitter-circled.png" alt="twitter-circled"/>
            </a>
            
            <a href="https://www.instagram.com" target="_blank" rel="noopener noreferrer">
                  <img width="30" height="30" src="https://img.icons8.com/ultraviolet/30/instagram-new.png" alt="instagram-new"/>
            </a>

            <a href="https://www.linkedin.com" target="_blank" rel="noopener noreferrer">
                  <img width="30" height="30" src="https://img.icons8.com/stickers/30/linkedin-circled.png" alt="linkedin-circled"/>
            </a>
            
            
            </section>
        </footer>
        </>
    )
}