
import { Link, useLoaderData } from "react-router-dom";

export async function loader({params}) {
    const response = await fetch(`${import.meta.env.VITE_API}/authors/${params.authorId}`);
    const author = await response.json(); 
    
    const bookResponse = await fetch(`${import.meta.env.VITE_API}/books?authorId=${params.authorId}`)
    const books =await bookResponse.json()
    
    return ({ author, books});
  }

export default function BooksForSingleAuthor (props) {
    const {author,books} = useLoaderData()
    console.log(author)
    return (
        <>
          <h1 className="font-bold text-center text-3xl py-5"> {author.authorName}'s Books </h1>
          <div>
            <ul className="grid grid-cols-4 gap-2">{
                books.map(book => {
                  return(
                    <Link to={`/books/${book._id}`}>
                     <li className="w-3/4 h-8/12 mt-5 ml-5 bg-orange-100" key= {book._id}>
                        <h3 className="font-bold text-slate-600 text-xl text-center"> {book.bookName}</h3>
                        <img src={book.image} className="w-full h-full object-cover"></img>
                      </li>
                    </Link>
                      
                  )
                })
             
                }
            </ul>
          </div>
        
        </>
    )
}