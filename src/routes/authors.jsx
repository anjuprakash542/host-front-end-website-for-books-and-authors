import { useEffect } from "react";
import { Link, useLoaderData } from "react-router-dom";

export async function loader() {
    const response = await fetch(`${import.meta.env.VITE_API}/authors`);
    const authors = await response.json()
    return { authors };
  }

export default function Authors (params) {
    useEffect(() => {
        document.body.style.backgroundImage = null;
        document.body.style.backgroundSize = null;
        document.body.style.backgroundRepeat = null;
    }, []);

    const {authors} = useLoaderData();
    return (
        <>
           <div> 
           
            <ul className="grid grid-cols-4 grid-rows-2 bg-slate-200">{

                authors.map(author => {
                 return (
                    <li className="mt-5 ml-5 bg-orange-100  " key={author._id} author={author}>
                      <Link to ={`/authors/${author._id}`}>
                           <img src={author.image} className="w-full h-full "></img>
                      </Link> 
                       
                    <h3 className="font-bold text-center text-xl"> {author.authorName} </h3>
                    </li>
                 )
                })
               
                }
            </ul>
         
           </div>
        </>
    )
}